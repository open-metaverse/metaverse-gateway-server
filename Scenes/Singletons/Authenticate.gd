extends Node


# Declare member variables here. Examples:
var network = NetworkedMultiplayerENet.new()
var ip = "127.0.0.1"
var port = 1911


# Called when the node enters the scene tree for the first time.
func _ready():
	connectToServer()


# Called when we connect to the server
func connectToServer():
	# create the network client using the remote server ip
	#  and the port
	network.create_client(ip, port)
	# we push this network to the node tree
	get_tree().set_network_peer(network)
	
	# connect the events for connection failed and succeeded
	# warning-ignore:return_value_discarded
	network.connect("connection_failed", self, "_on_connection_failed")
	# warning-ignore:return_value_discarded
	network.connect("connection_succeeded", self, "_on_connection_succeeded")
	
# Called when we failed to connect to the server
func _on_connection_failed():
	print("Failed to connect!")

# Called when we succeeded to connect to the server
func _on_connection_succeeded():
	print("Succesfully connected!")

func authenticatePlayer(username, password, player_id):
	print("send out authentication request")
	rpc_id(1, "authenticatePlayer", username, password, player_id)
	
remote func authenticationResult(result, player_id):
	print("Authentication result received and replaying to player login request")
	Gateway.returnLoginRequest(result, player_id)



















