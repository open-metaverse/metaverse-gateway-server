extends Node


# Declare member variables here. Examples:
var network = NetworkedMultiplayerENet.new()
var gateway_api = MultiplayerAPI.new()
var port = 1910
var max_players = 100


# Called when the node enters the scene tree for the first time.
func _ready():
	# starting the server instance
	startServer()

func _process(delta):
	if not custom_multiplayer.has_network_peer():
		return;
	custom_multiplayer.poll()


# Called when the server is started
func startServer():
	# setup the server using port and max_players
	network.create_server(port, max_players)
	# ???
	set_custom_multiplayer(gateway_api)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)
	
	print("Gateway server started")
	
	# attach events that get called whenever a user connects or disconnects
	#  to the server.
	network.connect("peer_connected", self, "_peer_connected")
	network.connect("peer_disconnected", self, "_peer_disconnected")

# Called whenever a player connects to the server
func _peer_connected(player_id):
	print("User " + str(player_id) + " connected")

# Called whenever a player disconnects from the server
func _peer_disconnected(player_id):
	print("User " + str(player_id) + " disconnected")

remote func loginRequest(username, password):
	print("Login request received")
	var player_id = custom_multiplayer.get_rpc_sender_id()
	Authenticate.authenticatePlayer(username, password, player_id)#

func returnLoginRequest(result, player_id):
	rpc_id(player_id, "returnLoginRequest", result)
	network.disconnect_peer(player_id)















